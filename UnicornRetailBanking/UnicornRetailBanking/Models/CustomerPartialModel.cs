﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;




namespace UnicornRetailBanking.Models
{
    /*--------------------- Partial Model For Customer Details Table--------------------------*/
   [MetadataType(typeof(tbl_Customer_1515934MetaData))]
    public partial class tbl_Customer_1515934
    {

        [NotMapped]
        public List<SelectListItem> lstState { get; set; }
        [NotMapped]
        public List<SelectListItem> lstCity { get; set; }
    }
   public partial class tbl_Customer_1515934MetaData
   {
       [Key]
       public int Customer_id { get; set; }
       [RegularExpression(@"^(\d{9})$", ErrorMessage = "Enter Valid SSNID")]
       [Display(Name = "SSN ID")]
       [Required(ErrorMessage = "Please Enter SSN ID")]
       public string SsnID { get; set; }
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Enter a Valid Name")]
        [RegularExpression("[\\dA-Za-z\\s-]+", ErrorMessage = "Enter a Valid Name")]
       [Display(Name = "Customer Name")]
       [Required(ErrorMessage = "Please Enter Name ")]
       public string Customername { get; set; }

       [Display(Name = "DoB")]
      
       [Required(ErrorMessage = "Please Enter Date of Birth ")]
       public Nullable<System.DateTime> Dob { get; set; }

       public Nullable<int> Age { get; set; }

       [Display(Name = "Email ID")]

       [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",ErrorMessage = "Enter a Valid Email ID")]
       [Required(ErrorMessage = "Please Enter Email ID ")]
       public string Email_id { get; set; }

       [Display(Name = "Contact Number")]
       [RegularExpression(@"^(\d{10})$", ErrorMessage = "Enter Valid Contact Number")]
       [Required(ErrorMessage = "Please Enter Contact Number ")]
       public Nullable<long> ContactNo { get; set; }

       [StringLength(50, MinimumLength = 3, ErrorMessage = "Enter a Valid Address")]
       [RegularExpression("[\\dA-Za-z\\s-]+", ErrorMessage = "Enter a Valid Address")]
       [Display(Name = "Address Line 1")]
       [Required(ErrorMessage = "Please Enter Address Line 1 ")]
       public string Address_1 { get; set; }
       [StringLength(50, MinimumLength = 3, ErrorMessage = "Enter a Valid Address")]
       [RegularExpression("[\\dA-Za-z\\s-]+", ErrorMessage = "Enter a Valid Address")]
       [Display(Name = "Address Line 2")]
       [Required(ErrorMessage = "Please Enter Address Line 2 ")]
       public string Address_2 { get; set; }

       [Display(Name = "City")]
       [Required(ErrorMessage = "Please Select City ")]
       public Nullable<int> CityId { get; set; }

       [Display(Name = "State")]
       [Required(ErrorMessage = "Please Select State")]
       public Nullable<int> StateID { get; set; }

       public string Customer_status { get; set; }
   }
   /*--------------------- Partial Model For View Customer Details ----------------------------*/
   [MetadataType(typeof(usp_view_cust_id_1499077_ResultMeta))]
   public partial class usp_view_cust_id_1499077_Result
   {
       /*--------------------- Declare Method For Getting States --------------------------*/
       [NotMapped]
       public List<SelectListItem> lstState { get; set; }
       /*--------------------- Declare Method For Getting Cities --------------------------*/
       [NotMapped]
       public List<SelectListItem> lstCity { get; set; }
   }
   public partial class usp_view_cust_id_1499077_ResultMeta
   {
       [Key]
       [Display(Name="Customer ID")]
       public int Customer_id { get; set; }
       [Display(Name = "Customer SSN ID")]
       public Nullable<long> SsnID { get; set; }
       [StringLength(20, MinimumLength = 3, ErrorMessage = "Enter a Valid Name")]
       [RegularExpression("[\\dA-Za-z\\s-]+", ErrorMessage = "Enter a Valid Name")]
        [Display(Name = "Customer Name")]
       public string Customername { get; set; }
       [DataType(DataType.Date)]
       [Display(Name = "Customer DOB")]
    
       [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
       public Nullable<System.DateTime> Dob { get; set; }
       [Display(Name = "Customer Age")]
       public Nullable<int> Age { get; set; }
        [EmailAddress(ErrorMessage = "Enter a Valid Email ID")]
       [Display(Name = "Customer EmailID")]
       [Required(ErrorMessage = "Please Enter Email")]
       public string Email_id { get; set; }
       [Display(Name = "Contact Number")]
       [RegularExpression(@"^(\d{10})$", ErrorMessage = "Enter Valid Contact Number")]
       [Required(ErrorMessage = "Please Enter Contact Number")]
       public Nullable<long> ContactNo { get; set; }
        [Required(ErrorMessage = "Please Enter Address Line1")]
       [StringLength(50, MinimumLength = 3, ErrorMessage = "Enter a Valid Address")]
       [RegularExpression("[\\dA-Za-z\\s-]+", ErrorMessage = "Enter a Valid Address")]
     
       [Display(Name = "Address Line1")]
       public string Address_1 { get; set; }
        [Required(ErrorMessage = "Please Enter Address Line2")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Enter a Valid Address")]
        [RegularExpression("[\\dA-Za-z\\s-]+", ErrorMessage = "Enter a Valid Address")]
       [Display(Name = "Address Line2")]
       public string Address_2 { get; set; }
        [Required(ErrorMessage = "Please Enter State")]
       [Display(Name = "State")]
       public Nullable<int> StateID { get; set; }
        [Required(ErrorMessage = "Please Enter City")]
       [Display(Name = "City")]
       public Nullable<int> CityId { get; set; }
       [Display(Name = "Customer Status")]
       public string Customer_status { get; set; }
      
   }
   /*--------------------- Partial Model For View Customer Details With ID -------------------------*/
    [MetadataType(typeof(usp_view_customer_1499077_ResultMetaData))]
   public partial class usp_view_customer_1499077_Result
   {

   }
   public partial class usp_view_customer_1499077_ResultMetaData
   {
       [Key]
       [Display(Name="Customer ID")]
       public int Customer_id { get; set; }
         [Display(Name = "Customer SSNID")]
       public Nullable<long> SsnID { get; set; }
         [Display(Name = "Customer Name")]
       public string Customername { get; set; }
         [Display(Name = "Email ID")]
       public string Email_id { get; set; }
         [Display(Name = "Contact Number")]
       public Nullable<long> ContactNo { get; set; }
         [Display(Name = "Customer Status")]
       public string Customer_status { get; set; }
         [Display(Name = "Address")]
       public string Address { get; set; }
   }
   /*--------------------------------Search Customer Partial Model------------------------*/
   [MetadataType(typeof(usp_search_customer_1506303_ResultMetaData))]
   public partial class usp_search_customer_1506303_Result
   {

   }
   public partial class usp_search_customer_1506303_ResultMetaData
   {
       [Key]
       [Display(Name = "Customer ID")]
       public int Customer_id { get; set; }
       [Display(Name = "Customer SSNID")]
       public Nullable<long> SsnID { get; set; }
       
       [Display(Name = "Customer Name")]
       public string Customername { get; set; }
       [Display(Name = "Customer Age")]
       public Nullable<int> Age { get; set; }
       [Display(Name = "Customer EmailID")]
       public string Email_id { get; set; }
       [Display(Name = "Conatct Number")]
       public Nullable<long> ContactNo { get; set; }
       [Display(Name = "Customer Status")]
       public string Customer_status { get; set; }
       [Display(Name = "City")]
       public string Cityname { get; set; }
       [Display(Name = "State")]
       public string statename { get; set; }
   }
}