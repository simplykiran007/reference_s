//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnicornRetailBanking.Models
{
    using System;
    
    public partial class usp_view_customer_1499077_Result
    {
        public int Customer_id { get; set; }
        public Nullable<long> SsnID { get; set; }
        public string Customername { get; set; }
        public string Email_id { get; set; }
        public Nullable<long> ContactNo { get; set; }
        public string Customer_status { get; set; }
        public string Address { get; set; }
    }
}
