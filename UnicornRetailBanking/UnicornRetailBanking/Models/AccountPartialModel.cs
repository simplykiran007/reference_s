﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UnicornRetailBanking.Models
{
    /*--------------------- Partial Model For Account Details Table ---------------------------*/
    [MetadataType(typeof(tbl_Account_1342268MetaData))]
    public partial class tbl_Account_1342268
    {
        [NotMapped]
        public List<SelectListItem> lstAcc { get; set; }
    }
    public partial class tbl_Account_1342268MetaData
    {
        [Key]
        [Display(Name = "Customer ID")]
        [Required(ErrorMessage="Enter Customer ID")]
        [RegularExpression(@"^(\d{9})$", ErrorMessage = "Enter Valid Customer ID")]
        public Nullable<int> Customer_id { get; set; }
        [Range(500,100000000,ErrorMessage="Minimum Deposit Amount Should be 500")]
        [Required(ErrorMessage = "Enter Deposit Amount")]
        [Display(Name = "Deposit Amount")]
        public Nullable<decimal> Balance { get; set; }

        public string AccountStatus { get; set; }

        public Nullable<System.DateTime> Created_on { get; set; }
        [Required(ErrorMessage = "Select Account Type")]
        [Display(Name = "Account Type")]
        public Nullable<int> Accounttype_id { get; set; }

        public Nullable<System.DateTime> Last_updatetime { get; set; }

        public long AccountID { get; set; }

        public virtual tbl_account_type tbl_account_type { get; set; }
        public virtual tbl_Customer_1515934 tbl_Customer_1515934 { get; set; }

    }
    [MetadataType(typeof(Usp_View_Account_1515934_ResultMetaData))]
    public partial class Usp_View_Account_1515934_Result
    {

    }
    public partial class Usp_View_Account_1515934_ResultMetaData
    {
        [Key]
        [Display(Name="Customer ID")]
        public int Customer_id { get; set; }
          [Display(Name = "Account ID")]
        public long AccountID { get; set; }
          [Display(Name = "Customer Name")]
        public string Customername { get; set; }
          [Display(Name = "Available Balance($)")]
        public Nullable<decimal> Balance { get; set; }
          [Display(Name = "Account Status")]
          public string AccountStatus { get; set; }
          [Display(Name = "Account Type")]
        public string accounttype_name { get; set; }
    }
    /*--------------------- Partial Model For View Account Details  ---------------------------*/
    [MetadataType(typeof(sp_searchAccountfinal_1499077_ResultMetaData))]
    public partial class sp_searchAccountfinal_1499077_Result
    {

    }
    public partial class sp_searchAccountfinal_1499077_ResultMetaData
    {
        [Key]
        [Display(Name="Customer ID")]
        public Nullable<int> Customer_id { get; set; }
         [Display(Name = "Available Balance")]
        public Nullable<decimal> Balance { get; set; }
          [Display(Name = "Account ID")]
        public long AccountID { get; set; }
         [Display(Name = "Account Type")]
        public string accounttype_name { get; set; }
    }
}