﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UnicornRetailBanking.Models
{
  [MetadataType(typeof(usp_login_1514614_ResultMetaData))]

    public partial class usp_login_1514614_Result
    {

    }
    public partial class usp_login_1514614_ResultMetaData
    {
        public int UserId { get; set; }
        [Display(Name="Username")]
        [StringLength(15, MinimumLength = 8, ErrorMessage = "Enter a Valid Username")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Enter a Valid Username")]
        [Required(ErrorMessage="Enter Username")]
        public string Username { get; set; }
         [Display(Name = "Password")]
         [RegularExpression(@"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{10,15}", ErrorMessage = "Enter Valid Password")]
        [Required(ErrorMessage = "Enter Password")]
        public string Password { get; set; }

        public Nullable<int> Role_id { get; set; }
    }
}