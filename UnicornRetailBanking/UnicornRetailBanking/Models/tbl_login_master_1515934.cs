//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnicornRetailBanking.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_login_master_1515934
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Nullable<int> Role_id { get; set; }
    }
}
