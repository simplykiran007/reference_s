﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UnicornRetailBanking.Models;

namespace UnicornRetailBanking.Controllers
{
    public class AccountOperationsController : Controller
    {
        AHD27_AMS120_Group2Entities objDB = new AHD27_AMS120_Group2Entities();
        // GET: /AccountOperations/


        /*---------------View all Transactions------------------*/

        public ActionResult Transacttable()
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "2")
                {
                    List<sp_ViewTrasact_table_1499077_Result> lsttransact = new List<sp_ViewTrasact_table_1499077_Result>();
                    lsttransact = objDB.sp_ViewTrasact_table_1499077().ToList();
                    return View(lsttransact);
                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                } 
         
            }
            catch (Exception)
            {
                
                 return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
         
        }


        /*--------------Deposit-------------*/

        public ActionResult DepositMoney(Int64? id)
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "2")
                {
                    if (id != null)
                    {
                        sp_deposit_withdraw_view_1499077_Result objAccount = new sp_deposit_withdraw_view_1499077_Result();
                        objAccount = objDB.sp_deposit_withdraw_view_1499077(id).FirstOrDefault();
                        return View(objAccount);
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                } 
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }           
           
        }

        [HttpPost]
        public ActionResult DepositMoney(decimal? amount, sp_deposit_withdraw_view_1499077_Result objAccount,string narration)
        {
            try
            {
                if (amount > 0 && amount<1000000000)
                {
                    int res = objDB.usp_deposit_1342268(amount, objAccount.AccountID, narration);
                    this.objDB.SaveChanges();
                    if (res > 0)
                    {
                        TempData["Message"] = "<script>alert('Successfully Deposited!!!');</script>";
                        return RedirectToAction("Transacttable");
                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Sorry Try Again');</script>";
                        return RedirectToAction("SearchAccount", "Account");
                    }
                }
                else
                {
                    TempData["Message"] = "<script>alert('Enter Valid Amount to Deposit');</script>";
                    return RedirectToAction("DepositMoney");
                }
                
                
            }

            catch (Exception )
            {
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           
        }



        /*--------------Withdrawl-------------*/

        public ActionResult WithdrawlMoney(Int64? id)
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "2")
                {
                    if (id != null)
                    {
                        sp_deposit_withdraw_view_1499077_Result objAccount = new sp_deposit_withdraw_view_1499077_Result();
                        objAccount = objDB.sp_deposit_withdraw_view_1499077(id).FirstOrDefault();
                        return View(objAccount);
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                } 
            }
            catch (Exception)
            {
                
                 return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
       
          
        }

        [HttpPost]
        public ActionResult WithdrawlMoney(decimal amount, sp_deposit_withdraw_view_1499077_Result objAccount, string narration)
        {
            try
            {
                if (amount > 0 )
                {
                    if (objAccount.Balance > amount)
                    {
                        var res = objDB.usp_withdraw_1495424(amount, objAccount.AccountID, narration);
                        this.objDB.SaveChanges();
                        if (res != 0)
                        {
                            TempData["Message"] = "<script>alert('Withdraw Successfull!!!');</script>";
                            return RedirectToAction("Transacttable");
                        }
                        else
                        {
                            TempData["Message"] = "<script>alert('Sorry Try Again');</script>";
                            return RedirectToAction("SearchAccount", "Account");
                        }
                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Sorry Insufficient Funds');</script>";
                        return RedirectToAction("WithdrawlMoney");
                    }
                
                }
                else
                {
                    TempData["Message"] = "<script>alert('Enter Valid Amount');</script>";
                    return RedirectToAction("WithdrawlMoney");
                }
             
               
            }

            catch (Exception)
            {
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
         
        }


        /*------------------Transfer------------------*/

        public ActionResult TransferMoney(Int64? id)
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "2")
                {
                    if (id != null)
                    {
                        sp_ViewAccountbyCustId_1499077_Result objCust = new sp_ViewAccountbyCustId_1499077_Result();
                        objCust = objDB.sp_ViewAccountbyCustId_1499077(id).FirstOrDefault();
                        return View(objCust);
                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Sorry Please Try Again');</script>";
                        return RedirectToAction("SearchAccount", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                } 
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }     
            
        }

        [HttpPost]
        public ActionResult TransferMoney(int source, int destination, decimal amount, string narration, sp_ViewAccountbyCustId_1499077_Result objCust)
        {
            try
            {
                sp_TransferView_1499077_Result objSourceAccount = new sp_TransferView_1499077_Result();
                objSourceAccount = objDB.sp_TransferView_1499077(objCust.Customer_id, source).FirstOrDefault();
                sp_TransferView_1499077_Result objDestinationAccount = new sp_TransferView_1499077_Result();
                objDestinationAccount = objDB.sp_TransferView_1499077(objCust.Customer_id, destination).FirstOrDefault();
                if (objSourceAccount != null && objDestinationAccount != null)
                {
                    if (objSourceAccount.accounttype_name.ToString() != objDestinationAccount.accounttype_name.ToString())
                    {
                        if (objDestinationAccount.AccountStatus.ToString() == "Active" && objSourceAccount.AccountStatus.ToString() == "Active")
                        {
                            if (objSourceAccount.Balance >= amount)
                            {
                                var res = objDB.usp_trans_funds_1499077(objSourceAccount.AccountID, objDestinationAccount.AccountID, amount, narration);
                                if (res != 0)
                                {
                                    TempData["Message"] = "<script>alert('Transfer Successfull!!!!!');</script>";
                                    return RedirectToAction("Transacttable");
                                }
                                else
                                {
                                    TempData["Message"] = "<script>alert('Sorry Please Try Again');</script>";
                                    return RedirectToAction("TransferMoney");
                                }

                            }
                            else
                            {
                                TempData["Message"] = "<script>alert('Sorry Insufficient Funds');</script>";
                                return RedirectToAction("TransferMoney");
                            }
                        }
                        else
                        {
                            TempData["Message"] = "<script>alert('Cannot Transfer Money Between Active and InActive Accounts');</script>";
                            return RedirectToAction("TransferMoney");
                        }
                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Transfer Cannot be Done with in Same Account');</script>";
                        return RedirectToAction("TransferMoney");
                    }
                }
                else
                {
                    TempData["Message"] = "<script>alert('Transfer Cannot be Done either Source or Destination Account Not Found');</script>";
                    return RedirectToAction("TransferMoney");
                }
                
               
            }

            catch (Exception)
            {

                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }


        }



        /*-----------------------------------------------------------------MiniStatement-------------------------------------------------------*/
       
        
        /*-------------------View Last 'N' Transactions----------------*/


        public ActionResult ViewRecentTransactions()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
            
        }

        [HttpPost]
        public ActionResult ViewRecentTransactions(Int64? Acc_ID)
        {
            try
            {
                usp_viewaccountbyid_status_1499077_Result objAcc = new usp_viewaccountbyid_status_1499077_Result();
                objAcc = objDB.usp_viewaccountbyid_status_1499077(Acc_ID).FirstOrDefault();
                if (objAcc != null)
                {
                    if (objAcc.AccountStatus.ToString() == "Active")
                    {
                        List<usp_last10_transactions_1515934_Result> lsttransact = new List<usp_last10_transactions_1515934_Result>();
                        lsttransact = objDB.usp_last10_transactions_1515934(Acc_ID).ToList();
                        if (lsttransact != null && lsttransact.Count > 0)
                        {
                            return View(lsttransact);
                        }
                        else
                        {
                            TempData["Message"] = "<script>alert('No Recent Transaction History');</script>";
                            return View();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Sorry Account is InActive');</script>";
                        return View();
                    }

                }
                else
                {
                    TempData["Message"] = "<script>alert('Enter Valid Account ID');</script>";
                    return View();
                }
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
          
         
        }

        /*-------------------View Transactions between Dates----------------*/

        public ActionResult ViewDurationTransaction()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
          
        }

        [HttpPost]
        public ActionResult ViewDurationTransaction(Int64? Acc_ID,DateTime? FromDate,DateTime? ToDate)
        {
            try
            {
                sp_searchAccountbyAccountId_1499077_Result objAcc = new sp_searchAccountbyAccountId_1499077_Result();
                objAcc = objDB.sp_searchAccountbyAccountId_1499077(Acc_ID).FirstOrDefault();
                if (objAcc != null)
                {
                    if (ToDate >= FromDate)
                    {
                        List<usp_Date_to_Date_transactions_1515934_Result> lstTrans = new List<usp_Date_to_Date_transactions_1515934_Result>();
                        lstTrans = objDB.usp_Date_to_Date_transactions_1515934(Acc_ID, FromDate, ToDate).ToList();
                        if (lstTrans != null && lstTrans.Count > 0)
                        {
                            return View(lstTrans);
                        }
                        else
                        {
                            TempData["Message"] = "<script>alert('No Recent Transaction History');</script>";
                            return View();
                        }
                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Please Check Start Date and End Date');</script>";
                        return View();
                    }
                }
                else
                {
                    TempData["Message"] = "<script>alert('Please Check the Account ID');</script>";
                    return View();
                }
              
            }
            catch (Exception)
            {
                
                 return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           
          
        }

        /*----------------DropDown----------*/

        private static List<SelectListItem> selectAccounts()
        {
            try
            {
                AHD27_AMS120_Group2Entities objDB = new AHD27_AMS120_Group2Entities();
                List<SelectListItem> items = new List<SelectListItem>();
                List<Sp_DropDown_Account_1515934_Result> lstAccountType = new List<Sp_DropDown_Account_1515934_Result>();
                lstAccountType = objDB.Sp_DropDown_Account_1515934().ToList();
                foreach (var acc in lstAccountType)
                {
                    items.Add(new SelectListItem
                    {
                        Text = acc.accounttype_name.ToString(),
                        Value = acc.accounttype_id.ToString()
                    });
                }
                return items;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        
        }


        public JsonResult GetAccount()
        {
            return Json(selectAccounts());
        }

    }
}
