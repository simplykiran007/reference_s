﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UnicornRetailBanking.Models;

namespace UnicornRetailBanking.Controllers
{
    public class CustomerDetailsController : Controller
    {

        Int64? c = 0;
        Int64? s = 0;

        AHD27_AMS120_Group2Entities objDB = new AHD27_AMS120_Group2Entities();
        // GET: CustomerDetails
        public ActionResult HomePage()
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "1")
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                }
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
        

        }




        /*------------------------View Customer Details-----------------------------*/


        public ActionResult ListCustomer()
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "1")
                {
                    List<usp_view_customer_1499077_Result> listCustomer = new List<usp_view_customer_1499077_Result>();
                    listCustomer = objDB.usp_view_customer_1499077().ToList();
                    return View(listCustomer);
                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                }
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
       
        }


        /*------------------------Search Customer Details-----------------------------*/

        public ActionResult SearchListCustomer()
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "1")
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                }
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
         
        }

        [HttpPost]
        public ActionResult SearchListCustomer(Int64? Cust_ID, Int64? Ssn_ID)
        {
            try
            {
                if ((Cust_ID != null && Ssn_ID == null) || (Cust_ID == null && Ssn_ID != null))
                {
                    List<usp_search_customer_1506303_Result> lstCustomer = new List<usp_search_customer_1506303_Result>();
                    c = Cust_ID;
                    s = Ssn_ID;
                    lstCustomer = objDB.usp_search_customer_1506303(c, s).ToList();
                    if (lstCustomer != null && lstCustomer.Count > 0)
                    {
                        return View(lstCustomer);
                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Customer Not Found');</script>";
                        return View();
                    }
                }
                else
                {
                    TempData["Message"] = "<script>alert('Please Enter Details');</script>";
                    return View();
                }
            }
            catch (Exception)
            {
                
               return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           
        }
     
        
        /*------------------------Add Customer Details-----------------------------*/
       
        
        public ActionResult AddCustomer()
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "1")
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                }
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           
        }
        [HttpPost]
        public ActionResult AddCustomer(tbl_Customer_1515934 objCustomer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    usp_ViewCustomerBySSNID_1506303_Result objCust = new usp_ViewCustomerBySSNID_1506303_Result();
                    objCust = objDB.usp_ViewCustomerBySSNID_1506303(objCustomer.SsnID).FirstOrDefault();
                    if (objCust == null)
                    {
                       
                            int res = objDB.usp_Customer_Add_1515934(objCustomer.SsnID, objCustomer.Customername, objCustomer.Dob, objCustomer.Email_id, objCustomer.ContactNo, objCustomer.Address_1, objCustomer.Address_2, objCustomer.CityId, objCustomer.StateID);
                            this.objDB.SaveChanges();
                            usp_ViewCustomerBySSNID_1506303_Result objCust1 = new usp_ViewCustomerBySSNID_1506303_Result();
                            objCust1 = objDB.usp_ViewCustomerBySSNID_1506303(objCustomer.SsnID).FirstOrDefault();
                            //TempData["Message"] = "<script>alert('Record Added Successfully');</script>";
                            ViewBag.Message = "Customer Data Added Successfully And Customer ID:" + objCust1.Customer_id;
                            //TempData["message"] = "CustomerId :" + objCustomer.Customer_id;
                            return RedirectToAction("ListCustomer");
                            //return View(objCustomer);
                        
                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Sorry Customer SSNID already Registered');</script>";
                        return View();
                    }
                }
                else
                {
                    TempData["Message"] = "<script>alert('Please Try Again');</script>";
                    return RedirectToAction("AddCustomer");
                }
            }
            catch (Exception)
            {
              
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           

        }
        /*------------------------Update Customer Details-----------------------------*/
        public ActionResult UpdateCustomer(int? id)
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "1")
                {
                    if (id != null)
                    {
                        usp_view_cust_id_1499077_Result objCustomer = new usp_view_cust_id_1499077_Result();
                        objCustomer = objDB.usp_view_cust_id_1499077(id).FirstOrDefault();
                        objCustomer.lstState = GetAllStateList();
                        objCustomer.lstCity = GetAllCityList(Convert.ToInt32(objCustomer.StateID));
                        return View(objCustomer);
                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Please Try Again');</script>";
                        return RedirectToAction("AddCustomer");
                    }
                }

                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                }

            }
            catch (Exception)
            {
                
               return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           
        }


        [HttpPost]
        public ActionResult UpdateCustomer(usp_view_cust_id_1499077_Result objCustomer)
        {
            try
            {
                if (ModelState.IsValid)
                {                                     
                    int res = objDB.usp_update_customer_1515934(objCustomer.Customer_id, objCustomer.Customername, objCustomer.Dob, objCustomer.Email_id, objCustomer.ContactNo, objCustomer.Address_1, objCustomer.Address_2, objCustomer.CityId, objCustomer.StateID);
                    this.objDB.SaveChanges();
                    TempData["Message"] = "<script>alert('Customer Details Updated Successfully');</script>";
                    return RedirectToAction("ListCustomer");

                }
                else
                {
                    TempData["Message"] = "<script>alert('Please Try Again');</script>";
                    return RedirectToAction("ListCustomer");
                }
                
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
        
           
        }
        /*------------------------Deactivate Customer Details-----------------------------*/
        public ActionResult Deactivate(int? id)
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "1")
                {
                    if (id != null)
                    {
                        int flag = 0;
                        List<sp_ViewAccountbyCustId_1499077_Result> objAcc = new List<sp_ViewAccountbyCustId_1499077_Result>();
                        objAcc = objDB.sp_ViewAccountbyCustId_1499077(id).ToList();
                        foreach (var k in objAcc)
                        {
                            if (k.Balance != 0)
                            {
                                flag++;
                            }
                        }
                        if (flag == 0)
                        {
                            usp_view_cust_id_1499077_Result objCustomer = new usp_view_cust_id_1499077_Result();
                            objCustomer = objDB.usp_view_cust_id_1499077(id).FirstOrDefault();
                            objCustomer.lstState = GetAllStateList();
                            objCustomer.lstCity = GetAllCityList(Convert.ToInt32(objCustomer.StateID));
                            return View(objCustomer);
                        }
                        else
                        {
                            TempData["Message"] = "<script>alert('Active Accounts are detected. Customer cannot be Deleted.');</script>";
                            return RedirectToAction("ListCustomer");
                        }

                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Please Try Again');</script>";
                        return RedirectToAction("ListCustomer");
                    }
                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                }
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           

        }
        [HttpPost]
        public ActionResult Deactivate(usp_view_cust_id_1499077_Result objCustomer)
        {
            try
            {
                int res = objDB.usp_del_customer_1515934(objCustomer.Customer_id);
                this.objDB.SaveChanges();
                if (res > 0)
                {
                    TempData["Message"] = "<script>alert('Customer Details Deleted Successfully');</script>";
                }
                else
                {
                    TempData["Message"] = "<script>alert('Sorry Please Try Again');</script>";
                }
                return RedirectToAction("ListCustomer");
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
          
        }
        /*------------------------Get All States-----------------------------*/
        private List<SelectListItem> GetAllStateList()
        {
            try
            {
                List<SelectListItem> items = new List<SelectListItem>();
                List<sp_drop_state_Result> lstState = new List<sp_drop_state_Result>();
                lstState = objDB.sp_drop_state().ToList();
                foreach (var st in lstState)
                {
                    items.Add(new SelectListItem
                    {
                        Text = st.statename.ToString(),
                        Value = st.stateid.ToString()
                    });
                }
                return items;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            

        }

        public JsonResult GetAllState()
        {
            return Json(GetAllStateList());
        }
        /*------------------------Get All Cities-----------------------------*/
        public List<SelectListItem> GetAllCityList(int StateID)
        {
            try
            {
                List<SelectListItem> items = new List<SelectListItem>();
                List<sp_drop_city_Result> lstCity = new List<sp_drop_city_Result>();
                lstCity = objDB.sp_drop_city(Convert.ToInt32(StateID)).ToList();
                foreach (var c in lstCity)
                {
                    items.Add(new SelectListItem
                    {
                        Text = c.Cityname.ToString(),
                        Value = c.Cityid.ToString()

                    });
                }
                return items;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
           
        }
        public JsonResult GetAllCity(string StateID)
        {
            return Json(GetAllCityList(Convert.ToInt32(StateID)));
        }
    }
}