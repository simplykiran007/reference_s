﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UnicornRetailBanking.Models;

namespace UnicornRetailBanking.Controllers
{
    public class AccountController : Controller
    {
        AHD27_AMS120_Group2Entities objDB = new AHD27_AMS120_Group2Entities();
        //
        // GET: /Account/

        Int64? c = 0;
        int? at = 0;
        Int64? a = 0;

        public ActionResult HomePage()
        {
            if (Session["Username"] != null && Session["RoleId"].ToString() == "2")
            {
                return View();
            }
            else
            {
                return RedirectToAction("LoginDetails", "Login");
            } 
            
        }


        //-------------View Customer Account--------------
        public ActionResult ViewCustomerAccount()
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "1")
                {
                    List<Usp_View_Account_1515934_Result> lstAccount = new List<Usp_View_Account_1515934_Result>();
                    lstAccount = objDB.Usp_View_Account_1515934().ToList();
                    return View(lstAccount);
                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                }
            }
            catch (Exception)
            {

                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
          
          
        }



        /*-------------------- Create Customer Account -----------------*/
        public ActionResult AddCustomerAccount(int ? id)
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "1")
                {
                    tbl_Account_1342268 objAccount = new tbl_Account_1342268();
                    objAccount.lstAcc = selectAccounts();
                    return View(objAccount);
                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                }
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           
          
        }

        [HttpPost]
        public ActionResult AddCustomerAccount(tbl_Account_1342268 objAccount)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    usp_view_cust_id_1499077_Result objCustomer = new usp_view_cust_id_1499077_Result();
                    objCustomer = objDB.usp_view_cust_id_1499077(objAccount.Customer_id).FirstOrDefault();
                    if (objCustomer != null)
                    {
                        if (objCustomer.Customer_status.ToString() == "ACTIVE")
                        {
                            usp_viewBy_Accountype_1506303_Result objStatus = new usp_viewBy_Accountype_1506303_Result();
                            objStatus = objDB.usp_viewBy_Accountype_1506303(objAccount.Customer_id, objAccount.Accounttype_id).FirstOrDefault();
                            if (objStatus == null || objStatus.Accounttype_id != objAccount.Accounttype_id)
                            {
                                int res = objDB.usp_Add_Account_1515934(objAccount.Customer_id, objAccount.Balance, objAccount.Accounttype_id);
                                                            
                                this.objDB.SaveChanges();

                               sp_TransferView_1499077_Result objAcc=new sp_TransferView_1499077_Result();
                                objAcc=objDB.sp_TransferView_1499077(objAccount.Customer_id,objAccount.Accounttype_id).FirstOrDefault();
                               
                                TempData["Message"] = "Account Created Succesfully!!! And Account ID:" + objAcc.AccountID;
                                return RedirectToAction("ViewCustomerAccount");
                            }
                            else
                            {
                                TempData["Message"] = "<script>alert('Customer already have this type of account.');</script>";
                                return RedirectToAction("AddCustomerAccount");
                            }
                        }
                        else
                        {

                            TempData["Message"] = "<script>alert('Customer is Inactive.Account cannot be created');</script>";
                            return RedirectToAction("AddCustomerAccount");
                        }
                    }

                    else
                    {
                        TempData["Message"] = "<script>alert('Customer not Found.Account cannot be created');</script>";
                        return RedirectToAction("AddCustomerAccount");
                    }

                }
                else
                {
                    return View();
                }

            }
            catch (Exception)
            {
                
                 return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
          
         
        }

        /*-------------------- Deactivate Customer Account -----------------*/
        public ActionResult DeactivateAccount(Int64? id)
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "1")
                {
                    if (id != null)
                    {
                        usp_viewaccountbyid_1515434_Result objAccount = new usp_viewaccountbyid_1515434_Result();
                        objAccount = objDB.usp_viewaccountbyid_1515434(id).FirstOrDefault();
                        if (objAccount.Balance == 0)
                        {
                            return View(objAccount);
                        }
                        else
                        {
                            TempData["Message"] = "<script>alert('Balance is not Zero. Cannot Delete Account');</script>";
                           
                            return RedirectToAction("ViewCustomerAccount");

                        }

                    }
                    else
                    {
                        return View();
                    }

                }
                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                } 
          
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           
        }

        [HttpPost]
        public ActionResult DeactivateAccount(usp_viewaccountbyid_1515434_Result objAccount)
        {
            try
            {
               
                    int res = objDB.usp_delete_account_1506303(objAccount.Customer_id);
                    this.objDB.SaveChanges();
                    if (res > 0)
                    {
                        TempData["Message"] = "<script>alert('Customer Account Deleted Successfully!!!!');</script>";
                        return RedirectToAction("ViewCustomerAccount");
                    }
                    else
                    {

                        TempData["Message"] = "<script>alert('Sorry please try again');</script>";
                        return RedirectToAction("ViewCustomerAccount");
                    }
                   
              
            }
            catch (Exception)
            {
                
                 return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           
        }

        /*----------------------------------------------Search Account----------------------------------------------------------*/

        public ActionResult SearchAccount()
        {
            try
            {
                if (Session["Username"] != null && Session["RoleId"].ToString() == "2")
                {
                    return View();
                }

                else
                {
                    return RedirectToAction("LoginDetails", "Login");
                } 
            }
            catch (Exception)
            {

                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
          
        }

        [HttpPost]
        public ActionResult SearchAccount(Int64? Cust_ID, int? Acc_typeID, Int64? Acc_ID)
        {
            try
            {
                List<sp_searchAccountfinal_1499077_Result> lstaccount = new List<sp_searchAccountfinal_1499077_Result>();

                c = Cust_ID;
                at = Acc_typeID;
                a = Acc_ID;
                if ((Cust_ID != null && Acc_typeID != 0 && Acc_ID == null) || (Cust_ID == null && Acc_typeID == 0 && Acc_ID != null))
                {


                    lstaccount = objDB.sp_searchAccountfinal_1499077(c, at, a).ToList();
                    if (lstaccount != null && lstaccount.Count > 0)
                    {
                        return View(lstaccount);
                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Customer Account not found. Please Check the Details you have entered.');</script>";
                        return View();
                    }


                }
                else
                {
                    TempData["Message"] = "<script>alert('Please Enter Details');</script>";
                    return View();
                }
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           
            
          
        }




       /*-------------------- Get Account Type-----------------*/
        private static List<SelectListItem> selectAccounts()
        {
            try
            {
                AHD27_AMS120_Group2Entities objDB = new AHD27_AMS120_Group2Entities();
                List<SelectListItem> items = new List<SelectListItem>();
                List<Sp_DropDown_Account_1515934_Result> lstAccountType = new List<Sp_DropDown_Account_1515934_Result>();
                lstAccountType = objDB.Sp_DropDown_Account_1515934().ToList();
                foreach (var acc in lstAccountType)
                {
                    items.Add(new SelectListItem
                    {
                        Text = acc.accounttype_name.ToString(),
                        Value = acc.accounttype_id.ToString()
                    });
                }
                return items;
            }
            catch (Exception ex)
            {

                throw ex;
            }
         
        }


        public JsonResult GetAccount()
        {
            return Json(selectAccounts());
        }

       











    }
}





