﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UnicornRetailBanking.Models;

namespace UnicornRetailBanking.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        AHD27_AMS120_Group2Entities objDB = new AHD27_AMS120_Group2Entities();
         public ActionResult LoginDetails()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoginDetails(usp_login_1514614_Result Loginobj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var log = (from user in objDB.tbl_login_master_1515934
                               where user.Username == Loginobj.Username && user.Password == Loginobj.Password
                               select new
                               {
                                   user.Username,
                                   user.Password,
                                   user.Role_id
                               }).ToList();
                    if (log.FirstOrDefault() != null)
                    {
                        Session["Username"] = log.FirstOrDefault().Username;
                        Session["RoleId"] = log.FirstOrDefault().Role_id;
                        int res = objDB.sp_userstore_master_1514614(Loginobj.Username);
                        if (Session["RoleId"].ToString() == "1")
                        {
                            ViewBag.Message = Session["Username"].ToString();
                            return RedirectToAction("HomePage", "CustomerDetails");
                            //TempData["Message"]="<script>alert('Welcome Execute');</script>";
                        }
                        else
                        {
                            return RedirectToAction("HomePage", "Account");
                            //TempData["Message"] = "<script>alert('Welcome cashier');</script>";
                        }
                    }
                    else
                    {
                        TempData["Message"] = "<script>alert('Enter Valid Credentials');</script>";
                    }
                }
                return View();
            }
            catch (Exception)
            {
                
                 return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
           
        }



        /*----------------------LogOut-------------------------*/
        public ActionResult Logout()
        {
            try
            {
                int res = objDB.usp_logout_session_1506303(Session["Username"].ToString());
                Session.Abandon();
                return RedirectToAction("Home", "WebHome");
            }
            catch (Exception)
            {
                
                return RedirectToAction("ExceptionHandle", "ExceptionHandler");
            }
         
        }
    }
}