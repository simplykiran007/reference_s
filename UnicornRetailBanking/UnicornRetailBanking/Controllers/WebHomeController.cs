﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UnicornRetailBanking.Models;

namespace UnicornRetailBanking.Controllers
{
    public class WebHomeController : Controller
    {
        AHD27_AMS120_Group2Entities objDB = new AHD27_AMS120_Group2Entities();
        // GET: WebHome




        /*--------------------------Home--------------------------------*/
        public ActionResult Home()
        {
            return View();
        }



        /*-------------------------------AboutUs-------------------------*/

        public ActionResult AboutUs()
        {
            return View();
        }


        /*---------------------------ContactUs-------------------------------*/

        public ActionResult ContactUs()
        {
             
            return View();
        
        }
        [HttpPost]
        public ActionResult ContactUs(string firstname)
        {
            TempData["Message"] = "<script>alert('Thankyou for your information. We will reach you as soon as possible.');</script>";
            return RedirectToAction("Home");
        }


        /*-----------------------Login-------------------------*/

        public ActionResult Login()
        {
            return RedirectToAction("LoginDetails", "Login");
        }
    }
}